// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: authForm
    width: parent.width
    height: 450

    Rectangle {
        id: firstName_base
        border.color: "black"
        radius: 1
        smooth: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width-50
        height: 50
    }

    TextInput {
        objectName: "AuthText"
        anchors.fill: firstName_base
        width: parent.width
        id: firstName
        color: "black"
        font.pixelSize: 30
        anchors.margins: 7
        selectByMouse: true
        clip: true
        onActiveFocusChanged: cursorPosition = (!activeFocus ? 0 : text.length)
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onPressed: firstName.focus = true   //mm Why did this stop working when TextInput was reparented to "content"?
    }
}
