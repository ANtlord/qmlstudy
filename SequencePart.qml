// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: parent.width
    height: 300


    Gradient {
        id: green
        GradientStop { position: 0.0; color: "#18b703"; }
        GradientStop { position: 1.0; color: "#174505"; }
    }
    Gradient {
        id: red
        GradientStop { position: 0.0; color: "#c47271"; }
        GradientStop { position: 1.0; color: "#c40101"; }
    }
    Gradient {
        id: blue
        GradientStop { position: 0.0; color: "#919aff"; }
        GradientStop { position: 1.0; color: "#0119d2"; }
    }
    Gradient {
        id: yellow
        GradientStop { position: 0.0; color: "#b9ff5a"; }
        GradientStop { position: 1.0; color: "#bfaf00"; }
    }

    children: [
        Choice {
            id: firstChoice
            y: 0
            gradient: red
            choice_type: "sequence"
            liecolor: "#ff0000"
        },
        Choice {
            id: secondChoice
            y: firstChoice.height+20
            gradient: green
            choice_type: "sequence"
            liecolor: "#00ff00"
        },
        Choice {
            id: thirdChoice
            y: secondChoice.y + secondChoice.height+20
            gradient: blue
            choice_type: "sequence"
            liecolor: "#0000ff"
        },
        Choice {
            id: fourthChoice
            y: thirdChoice.y + thirdChoice.height+20
            gradient: yellow
            choice_type: "sequence"
            liecolor: "#ffff00"
        }
    ]
    Sequence {
        id: sequence
        objectName: "sequence"
        y:fourthChoice.y+fourthChoice.height+20
    }
}
