// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: button
    width: 150
    height: 40
    radius: 20
    smooth: true
    border.width: 1
    border.color: "#111"
    property color lightGray: "#444"
    property color darkGray: "#333"

    TriGradient {
        id: onHoverGradient
        topColor: lightGray
        middleColor: darkGray
        bottomColor: lightGray
    }
    TriGradient {
        id: overHoverGradient
        topColor: Qt.darker(lightGray, 2)
        middleColor: Qt.darker(darkGray, 1.5)
        bottomColor: Qt.darker(lightGray, 2)
    }

    Text {
        id: labelButton
        text: "ok"
        anchors.centerIn: parent
        color: "white"
        font.bold: true
    }

    states: [
        State {
            name: "onClick"
            PropertyChanges {
                target: button
                gradient: onHoverGradient
            }
        },
        State {
            name: "offClick"
            PropertyChanges {
                target: button
                gradient: overHoverGradient
            }
        }
    ]

//    signal clicked
    MouseArea{
        id: buttonMouseArea
        anchors.fill: parent //область для приема событий от мышки будет занимать всю родительскую область
        onClicked: {
            switch (parent.objectName) {
                case "Skip": {
                    window.getNextQuestion();
                    break;
                }
                case "Response": {
                    window.responseQuestion();
                    break;
                }
                case "Authorise": {
                    window.authorise();
                    break;
                }
                case "HelpBtn": {
                    window.showHelp();
                    break;
                }
                default: {
                    console.log("Unknown button")
                }
            }
//            console.log("выбран следующий ответ")
        }
    }
    state: buttonMouseArea.pressed ? "offClick" : "onClick"
    property alias label: labelButton.text
}
