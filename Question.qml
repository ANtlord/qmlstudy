// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: questionBase
    width: parent.width-40
    height: question.height+20
    color: "#e0fbbd"
    radius: 10
    smooth: true
    border.color: "#334121"
    border.width: 1

    Text {
        id: question
        color: "black"
        font.family: "Arial"
        font.pixelSize: 15
//        font.weight: bold
        horizontalAlignment: Text.AlignHCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width-20
        wrapMode: Text.WordWrap
    }
    property alias label: question.text
    property alias fontsize: question.font.pixelSize
}
