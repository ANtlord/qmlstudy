// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: text_bar
    width: parent.width
    height: 300


    property alias label: response.text
    Rectangle {
        id: response_base
        border.color: "black"
        radius: 1
        smooth: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width-50
        height: 50
    }
    TextInput {
        anchors.fill: response_base
        width: parent.width
        id: response
        color: "black"
        font.pixelSize: 30
        anchors.margins: 7
        selectByMouse: true
        clip: true
        onActiveFocusChanged: cursorPosition = (!activeFocus ? 0 : text.length)
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onPressed: response.focus = true   //mm Why did this stop working when TextInput was reparented to "content"?
    }

}
