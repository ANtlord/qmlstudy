// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: btn_bar
    width: parent.width
    height: 62
//    anchors.centerIn: parent
    states: [
        State {
            name: "MainState"
            PropertyChanges { target:  skipBtn; opacity: 1 }
            PropertyChanges { target:  responseBtn; opacity: 1 }
            PropertyChanges { target:  authBtn; opacity: 0 }
        },
        State {
            name: "AuthState"
            PropertyChanges { target:  skipBtn; opacity: 0 }
            PropertyChanges { target:  responseBtn; opacity: 0 }
            PropertyChanges { target:  authBtn; opacity: 1 }
        },
        State {
            name: "ResumeState"
            PropertyChanges { target:  skipBtn; opacity: 0 }
            PropertyChanges { target:  responseBtn; opacity: 0 }
            PropertyChanges { target:  authBtn; opacity: 0 }
        }
    ]

    Button {
        id: skipBtn
        objectName: "Skip"
        label: "Пропустить"
        x: 70
        anchors.verticalCenter: parent.verticalCenter
    }
    Button {
        id: responseBtn
        objectName: "Response"
        label: "Ответить"
        x: parent.width-220
        anchors.verticalCenter: parent.verticalCenter
    }
    Button {
        id: authBtn
        objectName: "Authorise"
        label: "Начать тестирование"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Button {
        id: helpBtn
        x: mainForm.width - 50
//        anchors.right: parent.right
        objectName: "HelpBtn"
        label: "???"
        width: 40
        anchors.verticalCenter: parent.verticalCenter
//        anchors.horizontalCenter: parent.width
    }

//    MouseArea {
//        id: skip
//        anchors.fill: btn_bar
//        signal buttonClick()
//        onButtonClick: {
//            console.log('123');
//        }
//    }
}
