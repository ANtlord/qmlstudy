// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
//    id: colleration_bar
    width: parent.width
    height: 450

    children: [
        Rectangle {
            id:left
            property string side: "left"
            property bool clicked: false
            x:0
            y:0
            width: parent.width/2
            height: parent.height

            children: [
                Choice { id: lfirst; choice_type: "colleration"},
                Choice { id: lsecond; y:lfirst.height+20; choice_type: "colleration" },
                Choice { id: lthird; y:lsecond.y+lsecond.height+20; choice_type: "colleration" },
                Choice { id: lfourth; y:lthird.y+lthird.height+20; choice_type: "colleration" }
            ]
        },
        Rectangle {
            id:right
            property string side: "right"
            property bool clicked: false
            x:parent.width/2
            y:0
            width: parent.width/2
            height: parent.height
            children: [
                Choice { id: first; choice_type: "colleration" },
                Choice { id: second; y:first.height+20; choice_type: "colleration" },
                Choice { id: third; y:second.y+second.height+20; choice_type: "colleration" },
                Choice { id: fourth; y:third.y+third.height+20; choice_type: "colleration" },
                Choice { id: fifth; y:fourth.y+fourth.height+20; choice_type: "colleration" }
            ]
        }
    ]
    Rectangle {
        x: 0; y: left > right ? left.y+left.height : right.y+right.height
        id: popupHelp
        Text { id: helpText }
    }
    property alias label: helpText.text
}
