// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: choice_bar
    width: parent.width
    height: 450
    x: 0
    y: parent.Top

    children: [
        Choice {
            id: firstChoice
            y: 0
            state: ""
        },
        Choice {
            id: secondChoice
            y: firstChoice.height+20
            state: ""
        },
        Choice {
            id: thirdChoice
            y: secondChoice.y + secondChoice.height+20
            state: ""
        },
        Choice {
            id: fourthChoice
            y: thirdChoice.y + thirdChoice.height+20
            state: ""
        }
    ]
}
