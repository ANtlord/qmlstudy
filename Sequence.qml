// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: parent.width
    height: 20
    color: "black"
//    property list <SequenceIndicator> indicators: [
    children: [
        SequenceIndicator{id:first},
        SequenceIndicator{id:second; x:first.width},
        SequenceIndicator{id:third; x:second.x+first.width},
        SequenceIndicator{id:fourth; x:third.x+first.width}
    ]
}
