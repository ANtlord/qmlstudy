#-------------------------------------------------
#
# Project created by QtCreator 2012-10-10T23:12:47
#
#-------------------------------------------------

QT       += core gui declarative

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QMLstudy
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

OTHER_FILES += \
    MainForm.qml \
    Button.qml \
    TriGradient.qml \
    DoubleGradient.qml \
    Choice.qml \
    ChoicePart.qml \
    ButtonBar.qml \
    Question.qml \
    OptionPart.qml \
    TextResponse.qml \
    SequencePart.qml \
    SequenceIndicator.qml \
    Sequence.qml \
    CollerationPart.qml \
    AuthForm.qml
